module.exports = {
    hi: 'Привет!',
    openCrane: '💧 Открыть кран',
    closeCrane: '❌ Закрыть кран',
    selectAction: 'Выбери то что мне необходимо выполнить!',
    iWillOpen: 'Открываю кран!',
    iWillClose: 'Зкарываю кран!',
    openTemp: 'Открыть временно',
    openedTemp: 'Кран будет открыт на n минут(ы)',
    opened: 'Кран открыт',
    closed: 'Кран закрыт',
    writeTimeoutPlease: 'Введите на сколько минут открыть кран:',
    getStatus: 'Получить статус 📊',

    craneNotConnected: 'Кран не подключён!',
    error: 'Произошла ошибка'
}
