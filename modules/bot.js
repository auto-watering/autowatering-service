const { Telegraf, Markup, session } = require('telegraf')
const bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN)
const strings = require("../strings");
const ws = require('./ws');

let onWriteTime = false

module.exports = function init() {
    bot.start(async (ctx) => {
        ctx.reply(strings.hi)

        // await ctx.reply('clear', Markup.removeKeyboard())
        return await ctx.reply(strings.selectAction, Markup
            .keyboard([
                [strings.openCrane, strings.closeCrane],
                [strings.openTemp, strings.getStatus],
            ])
            .resize()
        )
    })

    bot.hears(strings.openCrane, async (ctx) => {
        ws.sendEvent('openCrane', 7200);
    })
    bot.hears(strings.closeCrane, async (ctx) => {
        ws.sendEvent('closeCrane');
    })
    bot.hears(strings.getStatus, async (stx) => {
        ws.sendEvent('getStatus');
    });
    bot.hears(strings.openTemp, async (ctx) => {
        ctx.reply(strings.writeTimeoutPlease)
        onWriteTime = true
    })
    bot.on('text', async (ctx) => {
        if (onWriteTime) {
            const minutes = Number(ctx.message.text)
            if (Number.isNaN(minutes)) {
                return ctx.reply('Введите число минут')
            }
            onWriteTime = false
            const opening = ws.sendEvent('openCrane', minutes*60);
            if (opening)
                return ctx.reply(strings.openedTemp.replace('n', minutes.toString()))
            else
                return ctx.reply(strings.error)
        }
    })

    bot.launch()

    return bot;
}
