const WebSocket = require('ws');
const strings = require("../strings");
const port = process.env.SERVER_PORT

let mainWs, mainBot

module.exports.init = function (bot) {
    mainBot = bot

    const wss = new WebSocket.Server({ port });

    wss.on('connection', function connection(ws) {
        mainWs = ws

        bot.telegram.sendMessage(process.env.MAIN_CHAT_ID, 'подключена плата')

        ws.on('message', function (message) {
            const determiner = message.indexOf(':');
            const messageType = message.substr(0, determiner);
            const messageData = message.substr(determiner + 1, message.length - 1);
            console.log(`MGS, : ${messageType}:${messageData}`);
            if (messageType === 'craneStatus') {
                switch (messageData) {
                    case 'opened':
                        bot.telegram.sendMessage(process.env.MAIN_CHAT_ID, strings.opened)
                        break
                    case 'closed':
                        bot.telegram.sendMessage(process.env.MAIN_CHAT_ID, strings.closed)
                        break
                }
            }
        });
    });

    return wss;
}

module.exports.sendEvent = function (type, val = 0) {
    if (!mainWs) {
        mainBot.telegram.sendMessage(process.env.MAIN_CHAT_ID, strings.craneNotConnected)
        return false;
    }
    mainWs.send(`${type.toString()}:${val.toString()}`);
    return true;
}
